import { createApp } from 'vue'
import App from './pages/App.vue'
import * as appRouter from './appRouter';

const app = createApp(App)
app.use(appRouter.routeConfig);
app.mount('#app')
