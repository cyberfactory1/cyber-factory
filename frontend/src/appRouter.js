import { createRouter, createWebHistory } from "vue-router";
import Home from "./components/Home.vue";

const routes = [
  { path: "/", component: Home, meta: { requiredAuth: false } }
];

export const routeConfig = createRouter({
  history: createWebHistory(),
  routes: routes,
});
