import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_FILTER, APP_INTERCEPTOR, RouterModule } from '@nestjs/core';

import { ApplicationsModule } from '@/applications/applications.module';
import { ArpTableModule } from '@/arp-table/arp-table.module';
import { AuthModule } from '@/auth/auth.module';
import { GlobalExceptionsFilter } from '@/common/filters/global-exceptions.filter';
import { CountPacketsModule } from '@/count-packets/count-packets.module';
import { CPSModule } from '@/cpss/cps.module';
import { DatabaseModule } from '@/database/database.module';
import { DevicesModule } from '@/devices/devices.module';
import { HealthModule } from '@/health/health.module';
import { HostsModule } from '@/hosts/hosts.module';
import { MonitorResourcesModule } from '@/monitor-resources/monitor-resources.module';
import { NetworkInterfacesModule } from '@/network-interfaces/network-interfaces.module';
import { PortsModule } from '@/ports/ports.module';
import { RolesModule } from '@/roles/roles.module';
import { ROUTES } from '@/routes';
import { SFCModule } from '@/sfcs/sfcs.module';
import { SystemServicesModule } from '@/system-services/system-services.module';
import { TokensModule } from '@/tokens/tokens.module';
import { UsersModule } from '@/users/users.module';

import { SitemapModule } from './sitemap/sitemap.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    TokensModule,
    RolesModule,
    AuthModule,
    CPSModule,
    DevicesModule,
    SFCModule,
    SystemServicesModule,
    ApplicationsModule,
    NetworkInterfacesModule,
    MonitorResourcesModule,
    ArpTableModule,
    CountPacketsModule,
    HostsModule,
    PortsModule,
    HealthModule,
    SitemapModule,
    RouterModule.register(ROUTES),
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
  ],
})
export class AppModule {}
