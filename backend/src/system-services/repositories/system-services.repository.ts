import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { SystemServiceEntity } from '@/system-services/dao/entity/system-service.entity';

export abstract class SystemServicesRepository extends AbstractRepository<SystemServiceEntity> {
  public abstract saveList(
    listSystemService: SystemServiceEntity[],
  ): Promise<void>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<SystemServiceEntity>>;

  public abstract findByUniqueFields(
    name: string,
    status: string,
    deviceId: number,
  ): Promise<SystemServiceEntity>;
}
