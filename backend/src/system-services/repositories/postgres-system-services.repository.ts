import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { SystemServiceEntity } from '@/system-services/dao/entity/system-service.entity';
import { SystemServicesRepository } from '@/system-services/repositories/system-services.repository';

@Injectable()
export class PostgresSystemServicesRepository extends SystemServicesRepository {
  constructor(
    @InjectRepository(SystemServiceEntity)
    private readonly systemServicesRepository: Repository<SystemServiceEntity>,
  ) {
    super(systemServicesRepository);
  }

  public async saveList(
    listSystemService: SystemServiceEntity[],
  ): Promise<void> {
    const queryBuilder = this.systemServicesRepository.createQueryBuilder();

    queryBuilder
      .insert()
      .into(SystemServiceEntity)
      .values(listSystemService)
      .orIgnore()
      .execute();

    return;
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<SystemServiceEntity>> {
    const queryBuilder =
      this.systemServicesRepository.createQueryBuilder('system_services');

    queryBuilder
      .orderBy('system_services.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }

  public async findByUniqueFields(
    name: string,
    status: string,
    deviceId: number,
  ): Promise<SystemServiceEntity> {
    return this.systemServicesRepository.findOne({
      where: {
        name,
        status,
        deviceId,
      },
    });
  }
}
