import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { PortEntity } from '@/ports/dao/entity/ports.entity';
import { PortsRepository } from '@/ports/repositories/ports.repository';

@Injectable()
export class PostgresPortsRepository extends PortsRepository {
  constructor(
    @InjectRepository(PortEntity)
    private readonly portRepository: Repository<PortEntity>,
  ) {
    super(portRepository);
  }

  public async saveList(listPorts: PortEntity[]): Promise<void> {
    const queryBuilder = this.portRepository.createQueryBuilder();

    queryBuilder
      .insert()
      .into(PortEntity)
      .values(listPorts)
      .orIgnore()
      .execute();

    return;
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<PortEntity>> {
    const queryBuilder = this.portRepository.createQueryBuilder('ports');

    queryBuilder
      .orderBy('ports.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }
}
