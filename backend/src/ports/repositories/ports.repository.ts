import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { PortEntity } from '@/ports/dao/entity/ports.entity';

export abstract class PortsRepository extends AbstractRepository<PortEntity> {
  public abstract saveList(listPorts: PortEntity[]): Promise<void>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<PortEntity>>;
}
