import { ApiProperty } from '@nestjs/swagger';

import { PortEntity } from '@/ports/dao/entity/ports.entity';

export class PortResponse {
  @ApiProperty()
  public readonly id: number;

  @ApiProperty()
  public readonly port: number;

  @ApiProperty()
  public readonly status: string;

  @ApiProperty()
  public readonly service: string;

  @ApiProperty()
  public readonly protocol: string;

  constructor(portEntity: PortEntity) {
    this.id = portEntity.id;
    this.port = portEntity.port;
    this.status = portEntity.status;
    this.service = portEntity.service;
    this.protocol = portEntity.protocol;
  }
}
