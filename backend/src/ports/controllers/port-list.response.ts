import { ApiProperty } from '@nestjs/swagger';

import { PaginatedResult } from '@/common/pagination/interfaces/pagination-result.interface';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { PortResponse } from '@/ports/controllers/port.response';
import { PortEntity } from '@/ports/dao/entity/ports.entity';

export class PortListResponse {
  @ApiProperty()
  public readonly meta: Partial<PageOptionsDto>;

  @ApiProperty({
    type: PortResponse,
    isArray: true,
  })
  public readonly items: PortResponse[];

  constructor({ meta, items }: PaginatedResult<PortEntity>) {
    this.meta = meta;
    this.items = items.map((host) => new PortResponse(host));
  }
}
