import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { ApiPaginatedResponse } from '@/common/pagination/api-pagination.response';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { PortResponse } from '@/ports/controllers/port.response';
import { PortListResponse } from '@/ports/controllers/port-list.response';
import { CreateListPortsDto, CreatePortDto } from '@/ports/dtos';
import { PortsService } from '@/ports/services/ports.service';

@ApiTags('Ports')
@Controller()
export class PortsController {
  constructor(private readonly portsService: PortsService) {}

  @Post('/')
  @ApiOkResponse({
    type: PortResponse,
  })
  public async create(@Body() dto: CreatePortDto): Promise<PortResponse> {
    return new PortResponse(await this.portsService.create(dto));
  }

  @Post('/upload-list')
  public async createList(@Body() dto: CreateListPortsDto): Promise<void> {
    await this.portsService.createList(dto);
  }

  @Get('/:id')
  @ApiOkResponse({
    type: PortResponse,
  })
  public async getById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<PortResponse> {
    return new PortResponse(await this.portsService.getOrFailById(id));
  }

  @Get('/')
  @ApiPaginatedResponse(PortResponse)
  public async getList(
    @Query() pagination: PageOptionsDto,
  ): Promise<PortListResponse> {
    return new PortListResponse(await this.portsService.getList(pagination));
  }

  @Delete('/:id')
  public async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.portsService.delete(id);
  }
}
