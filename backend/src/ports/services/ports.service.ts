import { Injectable } from '@nestjs/common';

import {
  getChunksList,
  MAX_SIZE_CHUNK,
} from '@/common//utils/get-chunks-list.utils';
import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { HostEntity } from '@/hosts/dao/entity/host.entity';
import { HostsService } from '@/hosts/services/hosts.service';
import { PortEntity } from '@/ports/dao/entity/ports.entity';
import {
  CreateListPortsDto,
  CreatePortDto,
  PortItemDto,
  UpdatePortDto,
} from '@/ports/dtos';
import { PortByIdNotFoundException } from '@/ports/exceptions';
import { PortsRepository } from '@/ports/repositories/ports.repository';

@Injectable()
export class PortsService {
  constructor(
    private readonly portsRepository: PortsRepository,
    private readonly hostsService: HostsService,
  ) {}

  public async create(dto: CreatePortDto): Promise<PortEntity> {
    const existingHost = await this.hostsService.getOrFailByIpAddress(
      dto.hostIpAddress,
    );

    const portEntity = this.buildPort(
      dto.port,
      dto.status,
      dto.service,
      dto.protocol,
      existingHost,
    );

    return this.portsRepository.save(portEntity);
  }

  public async createList(dto: CreateListPortsDto): Promise<void> {
    const { items, hostIpAddress } = dto;
    const existingHost = await this.hostsService.getOrFailByIpAddress(
      hostIpAddress,
    );

    if (items.length <= MAX_SIZE_CHUNK) {
      const listPorts = items.map((port) =>
        this.buildPort(
          port.port,
          port.status,
          port.service,
          port.protocol,
          existingHost,
        ),
      );

      await this.portsRepository.saveList(listPorts);

      return;
    }

    const chunksPorts = getChunksList<PortItemDto>(items, MAX_SIZE_CHUNK);

    for (const chunk of chunksPorts) {
      const listPorts = chunk.map((port) =>
        this.buildPort(
          port.port,
          port.status,
          port.service,
          port.protocol,
          existingHost,
        ),
      );

      await this.portsRepository.saveList(listPorts);
    }

    return;
  }

  public async update(id: number, dto: UpdatePortDto): Promise<PortEntity> {
    const existingPort = await this.getOrFailById(id);

    existingPort.port = dto.port;
    existingPort.service = dto.service;
    existingPort.status = dto.status;
    existingPort.protocol = dto.protocol;

    return this.portsRepository.save(existingPort);
  }

  public async findById(id: number): Promise<PortEntity> {
    return this.portsRepository.getById(id);
  }

  public async findAll(): Promise<PortEntity[]> {
    return this.portsRepository.findAll();
  }

  public async delete(id: number): Promise<void> {
    await this.getOrFailById(id);
    await this.portsRepository.delete(id);
  }

  public async getList(
    pagination: PageOptionsDto,
  ): Promise<PageDto<PortEntity>> {
    return this.portsRepository.findBy(pagination);
  }

  public async getOrFailById(id: number): Promise<PortEntity> {
    const existingPort = await this.findById(id);
    if (!existingPort) {
      throw new PortByIdNotFoundException();
    }

    return existingPort;
  }

  private buildPort(
    port: number,
    status: string,
    service: string,
    protocol: string,
    host: HostEntity,
  ): PortEntity {
    const portEntity = new PortEntity();

    portEntity.port = port;
    portEntity.status = status;
    portEntity.protocol = protocol;
    portEntity.service = service;
    portEntity.host = host;

    return portEntity;
  }
}
