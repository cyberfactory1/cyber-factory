import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { HostsModule } from '@/hosts/hosts.module';
import { PortsController } from '@/ports/controllers/ports.controller';
import { PortEntity } from '@/ports/dao/entity/ports.entity';
import { PortsRepository } from '@/ports/repositories/ports.repository';
import { PostgresPortsRepository } from '@/ports/repositories/postrgres-ports.repository';
import { PortsService } from '@/ports/services/ports.service';

@Module({
  imports: [TypeOrmModule.forFeature([PortEntity]), HostsModule],
  providers: [
    PortsService,
    {
      provide: PortsRepository,
      useClass: PostgresPortsRepository,
    },
  ],
  exports: [PortsService, PortsRepository],
  controllers: [PortsController],
})
export class PortsModule {}
