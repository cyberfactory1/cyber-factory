import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUniqueIndexForTablePorts1695233638274
  implements MigrationInterface
{
  name = 'CreateUniqueIndexForTablePorts1695233638274';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "ports" ADD CONSTRAINT "UQ_b84821436d05313491b20633ebc" 
      UNIQUE ("port", "status", "service", "protocol", "host_id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "ports" DROP CONSTRAINT "UQ_b84821436d05313491b20633ebc"`,
    );
  }
}
