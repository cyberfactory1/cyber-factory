import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreatePortsTable1693250543338 implements MigrationInterface {
  name = 'CreatePortsTable1693250543338';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "ports" (
        "id" SERIAL NOT NULL, 
        "port" integer NOT NULL, 
        "status" character varying NOT NULL, 
        "service" character varying NOT NULL, 
        "protocol" character varying NOT NULL, 
        "host_id" integer NOT NULL, 
        "created_date" TIMESTAMP NOT NULL DEFAULT now(), 
        CONSTRAINT "PK_291c9f372b1ce97c885e96f5ff4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "ports" ADD CONSTRAINT "FK_4d824ea041ec9c7c38f3309b756" 
      FOREIGN KEY ("host_id") REFERENCES "hosts"("id") 
      ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "ports" DROP CONSTRAINT "FK_4d824ea041ec9c7c38f3309b756"`,
    );
    await queryRunner.query(`DROP TABLE "ports"`);
  }
}
