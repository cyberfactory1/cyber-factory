import { Column, Entity, JoinColumn, ManyToOne, Unique } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { HostEntity } from '@/hosts/dao/entity/host.entity';

@Entity({ name: 'ports' })
@Unique(['port', 'status', 'service', 'protocol', 'hostId'])
export class PortEntity extends AbstractEntity {
  @Column()
  port: number;

  @Column({ type: 'varchar' })
  status: string;

  @Column({ type: 'varchar' })
  service: string;

  @Column({ type: 'varchar' })
  protocol: string;

  @Column()
  hostId!: number;

  @ManyToOne(() => HostEntity, (host) => host.ports)
  @JoinColumn()
  host!: HostEntity;
}
