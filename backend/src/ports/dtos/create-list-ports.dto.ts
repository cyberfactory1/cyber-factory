import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

import { PortItemDto } from '@/ports/dtos';

export class CreateListPortsDto {
  @ApiProperty({
    type: String,
    required: true,
  })
  @IsString()
  @IsDefined()
  public readonly hostIpAddress: string;

  public readonly items: PortItemDto[];
}
