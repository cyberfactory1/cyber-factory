import { PartialType } from '@nestjs/swagger';

import { PortItemDto } from '@/ports/dtos';

export class UpdatePortDto extends PartialType(PortItemDto) {}
