import { NotFoundException } from '@nestjs/common';

export class PortByIdNotFoundException extends NotFoundException {
  constructor() {
    super(`Port with id not found`);
  }
}
