import {
  DeepPartial,
  FindManyOptions,
  FindOneOptions,
  FindOptionsWhere,
  Repository,
} from 'typeorm';

import { AbstractEntity } from '../entities/abstract-entity';

export abstract class AbstractRepository<T extends AbstractEntity> {
  private model: Repository<T>;

  protected constructor(model: Repository<T>) {
    this.model = model;
  }

  public async save(entity: DeepPartial<T>): Promise<T> {
    return this.model.save(entity);
  }

  public async findByCondition(filterCondition: FindOneOptions<T>): Promise<T> {
    return await this.model.findOne(filterCondition);
  }

  public async findWithRelations(relations: FindManyOptions<T>): Promise<T[]> {
    return await this.model.find(relations);
  }

  public async findAll(options?: FindManyOptions<T>): Promise<T[]> {
    return await this.model.find(options);
  }

  public async getById(id: any): Promise<T> {
    const options: FindOptionsWhere<T> = {
      id: id,
    };

    return this.model.findOneBy(options);
  }

  public async update(entity: DeepPartial<T>): Promise<T> {
    return this.model.save(entity);
  }

  public async delete(id: number): Promise<void> {
    await this.model.delete(id);
  }
}
