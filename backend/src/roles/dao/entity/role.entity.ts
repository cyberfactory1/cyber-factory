import { Column, Entity, ManyToMany } from 'typeorm';

import { RolesEnum } from '@/common/constants/roles.enum';
import { AbstractEntity } from '@/common/entities/abstract-entity';
import { UserEntity } from '@/users/dao/entity/user.entity';

@Entity({ name: 'roles' })
export class RoleEntity extends AbstractEntity {
  @Column({ type: 'enum', enum: RolesEnum, default: RolesEnum.USER })
  name: RolesEnum;

  @ManyToMany(() => UserEntity, (UserEntity) => UserEntity.roles)
  users!: UserEntity[];
}
