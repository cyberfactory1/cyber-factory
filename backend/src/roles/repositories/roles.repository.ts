import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { RoleEntity } from '@/roles/dao/entity/role.entity';

export abstract class RolesRepository extends AbstractRepository<RoleEntity> {
  public abstract findByName(name: string): Promise<RoleEntity>;
}
