import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { RolesEnum } from '@/common/constants/roles.enum';
import { RoleEntity } from '@/roles/dao/entity/role.entity';
import { RolesRepository } from '@/roles/repositories/roles.repository';

@Injectable()
export class PostgresRolesRepository extends RolesRepository {
  constructor(
    @InjectRepository(RoleEntity)
    private readonly rolesRepository: Repository<RoleEntity>,
  ) {
    super(rolesRepository);
  }

  public findByName(name: string): Promise<RoleEntity> {
    return this.rolesRepository.findOne({
      where: {
        name: name as RolesEnum,
      },
    });
  }
}
