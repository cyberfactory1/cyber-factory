import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { CPSEntity } from '@/cpss/dao/entity/cps.entity';

export abstract class CPSRepository extends AbstractRepository<CPSEntity> {
  public abstract findByName(name: string): Promise<CPSEntity>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<CPSEntity>>;
}
