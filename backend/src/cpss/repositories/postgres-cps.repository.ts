import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { CPSEntity } from '@/cpss/dao/entity/cps.entity';
import { CPSRepository } from '@/cpss/repositories/cps.repository';

@Injectable()
export class PostgresCPSRepository extends CPSRepository {
  constructor(
    @InjectRepository(CPSEntity)
    private readonly cpsRepository: Repository<CPSEntity>,
  ) {
    super(cpsRepository);
  }

  public async findByName(name: string): Promise<CPSEntity> {
    return this.cpsRepository.findOne({
      where: {
        name,
      },
    });
  }

  public async findBy(pagination: PageOptionsDto): Promise<PageDto<CPSEntity>> {
    const queryBuilder = this.cpsRepository.createQueryBuilder('cps');

    queryBuilder
      .orderBy('cps.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }
}
