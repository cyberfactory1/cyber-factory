import { ApiProperty } from '@nestjs/swagger';

import { CPSEntity } from '@/cpss/dao/entity/cps.entity';

export class CPSResponse {
  @ApiProperty()
  public readonly id: number;

  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly description: string;

  constructor(cyberPhysicalSystemEntity: CPSEntity) {
    this.id = cyberPhysicalSystemEntity.id;
    this.name = cyberPhysicalSystemEntity.name;
    this.description = cyberPhysicalSystemEntity.description;
  }
}
