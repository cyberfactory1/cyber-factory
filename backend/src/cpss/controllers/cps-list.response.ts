import { ApiProperty } from '@nestjs/swagger';

import { PaginatedResult } from '@/common/pagination/interfaces/pagination-result.interface';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { CPSEntity } from '@/cpss/dao/entity/cps.entity';

import { CPSResponse } from './cps.response';

export class CPSListResponse {
  @ApiProperty()
  public readonly meta: Partial<PageOptionsDto>;

  @ApiProperty({
    type: CPSResponse,
    isArray: true,
  })
  public readonly items: CPSResponse[];

  constructor({ meta, items }: PaginatedResult<CPSEntity>) {
    this.meta = meta;
    this.items = items.map((cps) => new CPSResponse(cps));
  }
}
