import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { ApiPaginatedResponse } from '@/common/pagination/api-pagination.response';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { CPSResponse } from '@/cpss/controllers/cps.response';
import { CPSListResponse } from '@/cpss/controllers/cps-list.response';
import { CreateCPSDto } from '@/cpss/dtos';
import { CPSService } from '@/cpss/services/cps.service';

@ApiTags('Cyber Physical Systems')
@Controller()
export class CPSController {
  constructor(private readonly cpsService: CPSService) {}

  @Post('/')
  @ApiOkResponse({
    type: CPSResponse,
  })
  public async create(@Body() dto: CreateCPSDto): Promise<CPSResponse> {
    return new CPSResponse(await this.cpsService.create(dto));
  }

  @Get('/:id')
  @ApiOkResponse({
    type: CPSResponse,
  })
  public async getById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<CPSResponse> {
    console.log(id);
    return new CPSResponse(await this.cpsService.getOrFailById(id));
  }

  @Get('/')
  @ApiPaginatedResponse(CPSResponse)
  public async getList(
    @Query() pagination: PageOptionsDto,
  ): Promise<CPSListResponse> {
    return new CPSListResponse(await this.cpsService.getList(pagination));
  }

  @Delete('/:id')
  public async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.cpsService.delete(id);
  }
}
