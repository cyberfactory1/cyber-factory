import { NotFoundException } from '@nestjs/common';

export class CPSByIdNotFoundException extends NotFoundException {
  constructor() {
    super(`Cyber Physical System with id not found`);
  }
}
