import { BadRequestException } from '@nestjs/common';

export class CPSExistByNameException extends BadRequestException {
  constructor() {
    super('Cyber Physical System with this name already exists');
  }
}
