import { PartialType } from '@nestjs/swagger';

import { CreateCPSDto } from './create-cps.dto';

export class UpdateCPSDto extends PartialType(CreateCPSDto) {}
