import { Column, Entity, JoinColumn, ManyToMany, OneToMany } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';
import { UserEntity } from '@/users/dao/entity/user.entity';

@Entity({ name: 'cpss' })
export class CPSEntity extends AbstractEntity {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  description: string;

  @ManyToMany(() => UserEntity, (UserEntity) => UserEntity.cpss)
  users!: UserEntity[];

  @OneToMany(() => DeviceEntity, (device) => device.cps, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  devices!: DeviceEntity[];
}
