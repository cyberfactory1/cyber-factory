import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddFieldDescriptionInTableCFS1689582783295
  implements MigrationInterface
{
  name = 'AddFieldDescriptionInTableCFS1689582783295';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "cyber_physical_systems" ADD "description" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "cyber_physical_systems" DROP COLUMN "description"`,
    );
  }
}
