import { Injectable } from '@nestjs/common';

import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { CPSEntity } from '@/cpss/dao/entity/cps.entity';
import { CreateCPSDto, UpdateCPSDto } from '@/cpss/dtos';
import {
  CPSByIdNotFoundException,
  CPSExistByNameException,
} from '@/cpss/exceptions';
import { CPSRepository } from '@/cpss/repositories/cps.repository';
import { UsersService } from '@/users/services/users.service';

@Injectable()
export class CPSService {
  constructor(
    private readonly cpsRepository: CPSRepository,
    private readonly usersService: UsersService,
  ) {}

  public async create(dto: CreateCPSDto): Promise<CPSEntity> {
    const user = await this.usersService.getOrFailById(dto.userId);
    const existingCPS = await this.getByName(dto.name);

    if (existingCPS) {
      throw new CPSExistByNameException();
    }

    const cps = new CPSEntity();

    cps.name = dto.name;
    cps.description = dto.description;
    cps.users = [user];

    return this.cpsRepository.save(cps);
  }

  public async getByName(name: string): Promise<CPSEntity | null> {
    return this.cpsRepository.findByName(name);
  }

  public async update(id: number, dto: UpdateCPSDto): Promise<CPSEntity> {
    const cps = await this.getOrFailById(id);
    cps.name = dto.name;

    return this.cpsRepository.save(cps);
  }

  public async findById(id: number): Promise<CPSEntity> {
    return this.cpsRepository.getById(id);
  }

  public async findAll(): Promise<CPSEntity[]> {
    return this.cpsRepository.findAll();
  }

  public async delete(id: number): Promise<void> {
    await this.getOrFailById(id);
    await this.cpsRepository.delete(id);
  }

  public async getList(
    pagination: PageOptionsDto,
  ): Promise<PageDto<CPSEntity>> {
    return this.cpsRepository.findBy(pagination);
  }

  public async getOrFailById(id: number): Promise<CPSEntity> {
    const existingCPS = await this.findById(id);
    if (!existingCPS) {
      throw new CPSByIdNotFoundException();
    }

    return existingCPS;
  }
}
