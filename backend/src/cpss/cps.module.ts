import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CPSController } from '@/cpss/controllers/cps.controller';
import { CPSEntity } from '@/cpss/dao/entity/cps.entity';
import { CPSRepository } from '@/cpss/repositories/cps.repository';
import { PostgresCPSRepository } from '@/cpss/repositories/postgres-cps.repository';
import { CPSService } from '@/cpss/services/cps.service';
import { UsersModule } from '@/users/users.module';

@Module({
  imports: [TypeOrmModule.forFeature([CPSEntity]), UsersModule],
  providers: [
    CPSService,
    {
      provide: CPSRepository,
      useClass: PostgresCPSRepository,
    },
  ],
  exports: [CPSService, CPSRepository],
  controllers: [CPSController],
})
export class CPSModule {}
