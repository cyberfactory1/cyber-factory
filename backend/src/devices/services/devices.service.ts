import { Injectable } from '@nestjs/common';

import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { CPSService } from '@/cpss/services/cps.service';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';
import { CreateDeviceDto, UpdateDeviceDto } from '@/devices/dtos';
import {
  DeviceByIdNotFoundException,
  DeviceByMacAddressNotFoundException,
  DeviceExistException,
  DeviceWithMacAddressExistException,
} from '@/devices/exceptions';
import { DevicesRepository } from '@/devices/repositories/devices.repository';

@Injectable()
export class DevicesService {
  constructor(
    private readonly devicesRepository: DevicesRepository,
    private readonly cpsService: CPSService,
  ) {}

  public async create(dto: CreateDeviceDto): Promise<DeviceEntity> {
    await this.checkExistByMacAddress(dto.macAddress);

    const cyberPhysicalSystem = await this.cpsService.getOrFailById(
      dto.cyberPhysicalSystemId,
    );

    const existingDevice = await this.devicesRepository.findByNameOrAddresses({
      name: dto.name,
      ipAddress: dto.ipAddress,
      cyberPhysicalSystemId: dto.cyberPhysicalSystemId,
    });

    if (existingDevice) {
      throw new DeviceExistException();
    }

    const device = new DeviceEntity();

    device.ipAddress = dto.ipAddress;
    device.macAddress = dto.macAddress;
    device.name = dto.name;
    device.networkInterface = dto.networkInterface;
    device.cps = cyberPhysicalSystem;

    return this.devicesRepository.save(device);
  }

  public async getByName(name: string): Promise<DeviceEntity | null> {
    return this.devicesRepository.findByName(name);
  }

  public async update(id: number, dto: UpdateDeviceDto): Promise<DeviceEntity> {
    const device = await this.getOrFailById(id);

    device.ipAddress = dto.ipAddress;
    device.macAddress = dto.macAddress;
    device.name = dto.name;
    device.networkInterface = dto.networkInterface;

    return this.devicesRepository.save(device);
  }

  public async findById(id: number): Promise<DeviceEntity> {
    return this.devicesRepository.getById(id);
  }

  public async findAll(): Promise<DeviceEntity[]> {
    return this.devicesRepository.findAll();
  }

  public async findByMacAddress(macAddress: string): Promise<DeviceEntity> {
    return this.devicesRepository.findByMacAddress(macAddress);
  }

  public async delete(id: number): Promise<void> {
    await this.getOrFailById(id);
    await this.devicesRepository.delete(id);
  }

  public async getList(
    pagination: PageOptionsDto,
  ): Promise<PageDto<DeviceEntity>> {
    return this.devicesRepository.findBy(pagination);
  }

  public async getOrFailById(id: number): Promise<DeviceEntity> {
    const existingDevice = await this.findById(id);
    if (!existingDevice) {
      throw new DeviceByIdNotFoundException();
    }

    return existingDevice;
  }

  public async getOrFailByMacAddress(
    macAddress: string,
  ): Promise<DeviceEntity> {
    const existingDevice = await this.findByMacAddress(macAddress);
    if (!existingDevice) {
      throw new DeviceByMacAddressNotFoundException();
    }

    return existingDevice;
  }

  private async checkExistByMacAddress(macAddress: string): Promise<void> {
    const existingDevice = await this.devicesRepository.findByMacAddress(
      macAddress,
    );

    if (existingDevice) {
      throw new DeviceWithMacAddressExistException();
    }
  }
}
