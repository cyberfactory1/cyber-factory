import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';

import { NameOrAddressesOptions } from '../options';

export abstract class DevicesRepository extends AbstractRepository<DeviceEntity> {
  public abstract findByName(name: string): Promise<DeviceEntity>;

  public abstract findByMacAddress(macAddress: string): Promise<DeviceEntity>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<DeviceEntity>>;

  public abstract findByNameOrAddresses(
    options: NameOrAddressesOptions,
  ): Promise<DeviceEntity>;
}
