import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { CountPacketsEntity } from '@/count-packets/dao/entity/count-packets.entity';

export abstract class CountPacketsRepository extends AbstractRepository<CountPacketsEntity> {
  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<CountPacketsEntity>>;
}
