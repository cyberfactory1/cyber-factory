import {
  ArpTable,
  ArpTableItemEntity,
} from '@/arp-table/dao/entity/arp-table.entity';
import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';

export abstract class ArpTableRepository extends AbstractRepository<ArpTableItemEntity> {
  public abstract saveList(arpTable: ArpTable): Promise<void>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<ArpTableItemEntity>>;

  public abstract findByUniqueFields(
    ipAddress: string,
    deviceId: number,
  ): Promise<ArpTableItemEntity>;
}
