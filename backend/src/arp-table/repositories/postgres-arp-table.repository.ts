import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  ArpTable,
  ArpTableItemEntity,
} from '@/arp-table/dao/entity/arp-table.entity';
import { ArpTableRepository } from '@/arp-table/repositories/arp-table.repository';
import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';

@Injectable()
export class PostgresArpTableRepository extends ArpTableRepository {
  constructor(
    @InjectRepository(ArpTableItemEntity)
    private readonly arpTableRepository: Repository<ArpTableItemEntity>,
  ) {
    super(arpTableRepository);
  }

  public async saveList(arpTable: ArpTable): Promise<void> {
    const queryBuilder = this.arpTableRepository.createQueryBuilder();

    queryBuilder
      .insert()
      .into(ArpTableItemEntity)
      .values(arpTable)
      .orIgnore()
      .execute();

    return;
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<ArpTableItemEntity>> {
    const queryBuilder =
      this.arpTableRepository.createQueryBuilder('arp_table');

    queryBuilder
      .orderBy('arp_table.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }

  public async findByUniqueFields(
    ipAddress: string,
    deviceId: number,
  ): Promise<ArpTableItemEntity> {
    return this.arpTableRepository.findOne({
      where: {
        ipAddress,
        deviceId,
      },
    });
  }
}
