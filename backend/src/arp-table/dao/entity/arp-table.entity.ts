import { Column, Entity, JoinColumn, ManyToOne, Unique } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';

@Entity({ name: 'arp_table' })
@Unique(['ipAddress', 'deviceId'])
export class ArpTableItemEntity extends AbstractEntity {
  @Column({ type: 'varchar' })
  ipAddress: string;

  @Column({ type: 'varchar' })
  macAddress: string;

  @Column()
  deviceId!: number;

  @ManyToOne(() => DeviceEntity, (device) => device.arpTable)
  @JoinColumn()
  device!: DeviceEntity;
}

export type ArpTable = ArpTableItemEntity[];
