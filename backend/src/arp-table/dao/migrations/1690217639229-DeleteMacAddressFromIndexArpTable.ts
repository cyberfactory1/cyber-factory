import { MigrationInterface, QueryRunner } from 'typeorm';

export class DeleteMacAddressFromIndexArpTable1690217639229
  implements MigrationInterface
{
  name = 'DeleteMacAddressFromIndexArpTable1690217639229';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "arp_table" DROP CONSTRAINT "UQ_909be8808eccbdca58debf5069b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "arp_table" ADD CONSTRAINT "UQ_d2553c7e28644a0d499b95bb784" UNIQUE ("ip_address", "device_id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "arp_table" DROP CONSTRAINT "UQ_d2553c7e28644a0d499b95bb784"`,
    );
    await queryRunner.query(
      `ALTER TABLE "arp_table" ADD CONSTRAINT "UQ_909be8808eccbdca58debf5069b" UNIQUE ("ip_address", "mac_address", "device_id")`,
    );
  }
}
