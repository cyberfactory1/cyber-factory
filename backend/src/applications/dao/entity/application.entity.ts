import { Column, Entity, JoinColumn, ManyToOne, Unique } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';

@Entity({ name: 'applications' })
@Unique(['name', 'version', 'deviceId'])
export class ApplicationEntity extends AbstractEntity {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  version: string;

  @Column({ type: 'varchar' })
  description: string;

  @Column()
  deviceId!: number;

  @ManyToOne(() => DeviceEntity, (device) => device.applications)
  @JoinColumn()
  device!: DeviceEntity;
}
