import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ApplicationEntity } from '@/applications/dao/entity/application.entity';
import { ApplicationsRepository } from '@/applications/repositories/applications.repository';
import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';

@Injectable()
export class PostgresApplicationsRepository extends ApplicationsRepository {
  constructor(
    @InjectRepository(ApplicationEntity)
    private readonly applicationsRepository: Repository<ApplicationEntity>,
  ) {
    super(applicationsRepository);
  }

  public async saveList(listApplications: ApplicationEntity[]): Promise<void> {
    const queryBuilder = this.applicationsRepository.createQueryBuilder();

    queryBuilder
      .insert()
      .into(ApplicationEntity)
      .values(listApplications)
      .orIgnore()
      .execute();

    return;
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<ApplicationEntity>> {
    const queryBuilder =
      this.applicationsRepository.createQueryBuilder('applications');

    queryBuilder
      .orderBy('applications.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }

  public async findByUniqueFields(
    name: string,
    version: string,
    deviceId: number,
  ): Promise<ApplicationEntity> {
    return this.applicationsRepository.findOne({
      where: {
        name,
        version,
        deviceId,
      },
    });
  }
}
