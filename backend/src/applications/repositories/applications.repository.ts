import { ApplicationEntity } from '@/applications/dao/entity/application.entity';
import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';

export abstract class ApplicationsRepository extends AbstractRepository<ApplicationEntity> {
  public abstract saveList(
    listApplications: ApplicationEntity[],
  ): Promise<void>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<ApplicationEntity>>;

  public abstract findByUniqueFields(
    name: string,
    version: string,
    deviceId: number,
  ): Promise<ApplicationEntity>;
}
