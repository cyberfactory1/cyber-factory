import { Injectable } from '@nestjs/common';

import { ApplicationEntity } from '@/applications/dao/entity/application.entity';
import {
  ApplicationItemDto,
  CreateApplicationDto,
  CreateListApplicationsDto,
  UpdateApplicationDto,
} from '@/applications/dtos';
import { ApplicationByIdNotFoundException } from '@/applications/exceptions';
import { ApplicationsRepository } from '@/applications/repositories/applications.repository';
import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import {
  getChunksList,
  MAX_SIZE_CHUNK,
} from '@/common/utils/get-chunks-list.utils';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';
import { DevicesService } from '@/devices/services/devices.service';

@Injectable()
export class ApplicationsService {
  constructor(
    private readonly applicationsRepository: ApplicationsRepository,
    private readonly devicesService: DevicesService,
  ) {}

  public async create(dto: CreateApplicationDto): Promise<ApplicationEntity> {
    const existingDevice = await this.devicesService.getOrFailByMacAddress(
      dto.deviceMacAddress,
    );

    const existingApplication =
      await this.applicationsRepository.findByUniqueFields(
        dto.name,
        dto.version,
        existingDevice.id,
      );

    if (existingApplication) {
      return existingApplication;
    }

    const application = this.buildApplication(
      dto.name,
      dto.version,
      dto.description,
      existingDevice,
    );

    return this.applicationsRepository.save(application);
  }

  public async createList(dto: CreateListApplicationsDto): Promise<void> {
    const { items, deviceMacAddress } = dto;
    const existingDevice = await this.devicesService.getOrFailByMacAddress(
      deviceMacAddress,
    );

    if (items.length <= MAX_SIZE_CHUNK) {
      const listApplications = items.map((application) =>
        this.buildApplication(
          application.name,
          application.version,
          application.description,
          existingDevice,
        ),
      );

      await this.applicationsRepository.saveList(listApplications);

      return;
    }

    const chunksApplications = getChunksList<ApplicationItemDto>(
      items,
      MAX_SIZE_CHUNK,
    );

    for (const chunk of chunksApplications) {
      const listApplications = chunk.map((application) =>
        this.buildApplication(
          application.name,
          application.version,
          application.description,
          existingDevice,
        ),
      );

      await this.applicationsRepository.saveList(listApplications);
    }

    return;
  }

  public async update(
    id: number,
    dto: UpdateApplicationDto,
  ): Promise<ApplicationEntity> {
    const existingApplication = await this.getOrFailById(id);

    existingApplication.name = dto.name;
    existingApplication.version = dto.version;
    existingApplication.description = dto.description;

    return this.applicationsRepository.save(existingApplication);
  }

  public async findById(id: number): Promise<ApplicationEntity> {
    return this.applicationsRepository.getById(id);
  }

  public async findAll(): Promise<ApplicationEntity[]> {
    return this.applicationsRepository.findAll();
  }

  public async delete(id: number): Promise<void> {
    await this.getOrFailById(id);
    await this.applicationsRepository.delete(id);
  }

  public async getList(
    pagination: PageOptionsDto,
  ): Promise<PageDto<ApplicationEntity>> {
    return this.applicationsRepository.findBy(pagination);
  }

  public async getOrFailById(id: number): Promise<ApplicationEntity> {
    const existingApplication = await this.findById(id);
    if (!existingApplication) {
      throw new ApplicationByIdNotFoundException();
    }

    return existingApplication;
  }

  private buildApplication(
    name: string,
    version: string,
    description: string,
    device: DeviceEntity,
  ): ApplicationEntity {
    const application = new ApplicationEntity();

    application.name = name;
    application.version = version;
    application.description = description;
    application.device = device;

    return application;
  }
}
