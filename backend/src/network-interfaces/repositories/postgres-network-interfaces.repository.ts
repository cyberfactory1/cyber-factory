import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { NetworkInterfacesRepository } from '@/network-interfaces/repositories/network-interfaces.repository';

import { NetworkInterfaceEntity } from '../dao/entity/network-interface.entity';

@Injectable()
export class PostgresNetworkInterfacesRepository extends NetworkInterfacesRepository {
  constructor(
    @InjectRepository(NetworkInterfaceEntity)
    private readonly networkInterfaceRepository: Repository<NetworkInterfaceEntity>,
  ) {
    super(networkInterfaceRepository);
  }

  public async saveList(
    listNetworkInterfaces: NetworkInterfaceEntity[],
  ): Promise<void> {
    const queryBuilder = this.networkInterfaceRepository.createQueryBuilder();

    queryBuilder
      .insert()
      .into(NetworkInterfaceEntity)
      .values(listNetworkInterfaces)
      .orIgnore()
      .execute();

    return;
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<NetworkInterfaceEntity>> {
    const queryBuilder =
      this.networkInterfaceRepository.createQueryBuilder('network_interfaces');

    queryBuilder
      .orderBy('network_interfaces.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }

  public findByName(name: string): Promise<NetworkInterfaceEntity> {
    return this.networkInterfaceRepository.findOne({
      where: {
        name,
      },
    });
  }
}
