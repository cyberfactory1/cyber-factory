import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { NetworkInterfaceEntity } from '@/network-interfaces/dao/entity/network-interface.entity';

export abstract class NetworkInterfacesRepository extends AbstractRepository<NetworkInterfaceEntity> {
  public abstract saveList(
    listNetworkInterfaces: NetworkInterfaceEntity[],
  ): Promise<void>;

  public abstract findByName(name: string): Promise<NetworkInterfaceEntity>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<NetworkInterfaceEntity>>;
}
