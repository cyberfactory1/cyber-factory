import { Column, Entity, JoinColumn, ManyToOne, Unique } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';

@Entity({ name: 'network_interfaces' })
@Unique(['name', 'deviceId'])
export class NetworkInterfaceEntity extends AbstractEntity {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  ipAddress: string;

  @Column()
  deviceId!: number;

  @ManyToOne(() => DeviceEntity, (device) => device.networkInterfaces)
  @JoinColumn()
  device!: DeviceEntity;
}
