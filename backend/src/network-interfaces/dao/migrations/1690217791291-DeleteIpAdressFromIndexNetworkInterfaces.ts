import { MigrationInterface, QueryRunner } from 'typeorm';

export class DeleteIpAdressFromIndexNetworkInterfaces1690217791291
  implements MigrationInterface
{
  name = 'DeleteIpAdressFromIndexNetworkInterfaces1690217791291';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "network_interfaces" DROP CONSTRAINT "UQ_884c451715628cc292355314acb"`,
    );
    await queryRunner.query(
      `ALTER TABLE "network_interfaces" ADD CONSTRAINT "UQ_6296191fd8f2dd82d8cca0c0e14" UNIQUE ("name", "device_id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "network_interfaces" DROP CONSTRAINT "UQ_6296191fd8f2dd82d8cca0c0e14"`,
    );
    await queryRunner.query(
      `ALTER TABLE "network_interfaces" ADD CONSTRAINT "UQ_884c451715628cc292355314acb" UNIQUE ("name", "ip_address", "device_id")`,
    );
  }
}
