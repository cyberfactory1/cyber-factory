import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { MonitorResourcesRepository } from '@/monitor-resources/repositories/monitor-resources.repository';

import { MonitorResourceEntity } from '../dao/entity/monitor-resource.entity';

@Injectable()
export class PostgresMonitorResourcesRepository extends MonitorResourcesRepository {
  constructor(
    @InjectRepository(MonitorResourceEntity)
    private readonly monitorResourcesRepository: Repository<MonitorResourceEntity>,
  ) {
    super(monitorResourcesRepository);
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<MonitorResourceEntity>> {
    const queryBuilder =
      this.monitorResourcesRepository.createQueryBuilder('monitor_resources');

    queryBuilder
      .orderBy('monitor_resources.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }
}
