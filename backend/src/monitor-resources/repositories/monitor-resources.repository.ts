import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { MonitorResourceEntity } from '@/monitor-resources/dao/entity/monitor-resource.entity';

export abstract class MonitorResourcesRepository extends AbstractRepository<MonitorResourceEntity> {
  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<MonitorResourceEntity>>;
}
