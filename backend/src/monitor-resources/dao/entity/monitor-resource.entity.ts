import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';

@Entity({ name: 'monitor_resources' })
export class MonitorResourceEntity extends AbstractEntity {
  @Column({ type: 'decimal' })
  cpuLoad: number;

  @Column({ type: 'decimal' })
  cpuUsage: number;

  @Column({ type: 'decimal' })
  cpuAvgLoad: number;

  @Column({ type: 'decimal' })
  cpuTemperature: number;

  @Column({ type: 'decimal' })
  ramUsage: number;

  @Column({ type: 'decimal' })
  swapUsage: number;

  @Column({ type: 'decimal' })
  diskUsage: number;

  @Column({ type: 'varchar' })
  uptime: string;

  @Column()
  deviceId!: number;

  @ManyToOne(() => DeviceEntity, (device) => device.monitorResources)
  @JoinColumn()
  device!: DeviceEntity;
}
