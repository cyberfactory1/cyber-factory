import { ApiProperty } from '@nestjs/swagger';

import { PaginatedResult } from '@/common/pagination/interfaces/pagination-result.interface';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { SFCResponse } from '@/sfcs/controllers/sfc.response';
import { SFCEntity } from '@/sfcs/dao/entity/sfc.entity';

export class SFCListResponse {
  @ApiProperty()
  public readonly meta: Partial<PageOptionsDto>;

  @ApiProperty({
    type: SFCResponse,
    isArray: true,
  })
  public readonly items: SFCResponse[];

  constructor({ meta, items }: PaginatedResult<SFCEntity>) {
    this.meta = meta;
    this.items = items.map(
      (structuralFunctionalCharacteristic) =>
        new SFCResponse(structuralFunctionalCharacteristic),
    );
  }
}
