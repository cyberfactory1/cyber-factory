import { ApiProperty } from '@nestjs/swagger';

import { SFCEntity } from '@/sfcs/dao/entity/sfc.entity';

export class SFCResponse {
  @ApiProperty()
  public readonly id: number;

  @ApiProperty()
  public readonly name: string;

  @ApiProperty()
  public readonly version: string;

  constructor(sfc: SFCEntity) {
    this.id = sfc.id;
    this.name = sfc.name;
    this.version = sfc.version;
  }
}
