import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { ApiPaginatedResponse } from '@/common/pagination/api-pagination.response';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { SFCResponse } from '@/sfcs/controllers/sfc.response';
import { SFCListResponse } from '@/sfcs/controllers/sfcs-list.response';
import { CreateListSFCDto, CreateSFCDto } from '@/sfcs/dtos';
import { SFCService } from '@/sfcs/services/sfc.service';

@ApiTags('Structural Functional Characteristics')
@Controller()
export class SFCController {
  constructor(private readonly sfcService: SFCService) {}

  @Post('/')
  @ApiOkResponse({
    type: SFCResponse,
  })
  public async create(@Body() dto: CreateSFCDto): Promise<SFCResponse> {
    return new SFCResponse(await this.sfcService.create(dto));
  }

  @Post('/upload-list')
  public async createList(@Body() dto: CreateListSFCDto): Promise<void> {
    await this.sfcService.createList(dto);
  }

  @Get('/:id')
  @ApiOkResponse({
    type: SFCResponse,
  })
  public async getById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<SFCResponse> {
    console.log(id);
    return new SFCResponse(await this.sfcService.getOrFailById(id));
  }

  @Get('/')
  @ApiPaginatedResponse(SFCResponse)
  public async getList(
    @Query() pagination: PageOptionsDto,
  ): Promise<SFCListResponse> {
    return new SFCListResponse(await this.sfcService.getList(pagination));
  }

  @Delete('/:id')
  public async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.sfcService.delete(id);
  }
}
