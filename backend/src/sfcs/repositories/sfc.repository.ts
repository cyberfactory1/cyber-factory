import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { SFCEntity } from '@/sfcs/dao/entity/sfc.entity';

export abstract class SFCRepository extends AbstractRepository<SFCEntity> {
  public abstract saveList(listSFC: SFCEntity[]): Promise<void>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<SFCEntity>>;
}
