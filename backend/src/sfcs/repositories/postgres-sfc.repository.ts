import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';

import { SFCEntity } from '../dao/entity/sfc.entity';
import { SFCRepository } from './sfc.repository';

@Injectable()
export class PostgresSFCRepository extends SFCRepository {
  constructor(
    @InjectRepository(SFCEntity)
    private readonly sfcRepository: Repository<SFCEntity>,
  ) {
    super(sfcRepository);
  }

  public saveList(listSFC: SFCEntity[]): Promise<void> {
    const queryBuilder = this.sfcRepository.createQueryBuilder();

    queryBuilder.insert().into(SFCEntity).values(listSFC).orIgnore().execute();

    return;
  }

  public async findBy(pagination: PageOptionsDto): Promise<PageDto<SFCEntity>> {
    const queryBuilder = this.sfcRepository.createQueryBuilder('sfc');

    queryBuilder
      .orderBy('sfc.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }
}
