import { Injectable } from '@nestjs/common';

import {
  getChunksList,
  MAX_SIZE_CHUNK,
} from '@/common//utils/get-chunks-list.utils';
import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';
import { DevicesService } from '@/devices/services/devices.service';
import { SFCEntity } from '@/sfcs/dao/entity/sfc.entity';
import {
  CreateListSFCDto,
  CreateSFCDto,
  SFCItemDto,
  UpdateSFCDto,
} from '@/sfcs/dtos';
import { SFCByIdNotFoundException } from '@/sfcs/exceptions';
import { SFCRepository } from '@/sfcs/repositories/sfc.repository';
@Injectable()
export class SFCService {
  constructor(
    private readonly sfcRepository: SFCRepository,
    private readonly devicesService: DevicesService,
  ) {}

  public async create(dto: CreateSFCDto): Promise<SFCEntity> {
    const existingDevice = await this.devicesService.getOrFailByMacAddress(
      dto.deviceMacAddress,
    );

    const sfc = this.buildSFC(dto.name, dto.version, existingDevice);

    return this.sfcRepository.save(sfc);
  }

  public async createList(dto: CreateListSFCDto): Promise<void> {
    const { items, deviceMacAddress } = dto;
    const existDevice = await this.devicesService.getOrFailByMacAddress(
      deviceMacAddress,
    );

    if (items.length <= MAX_SIZE_CHUNK) {
      const listSFC = items.map((sfc) =>
        this.buildSFC(sfc.name, sfc.version, existDevice),
      );

      await this.sfcRepository.saveList(listSFC);

      return;
    }

    const chunksSFC = getChunksList<SFCItemDto>(items, MAX_SIZE_CHUNK);

    for (const chunk of chunksSFC) {
      const listSFC = chunk.map((sfc) =>
        this.buildSFC(sfc.name, sfc.version, existDevice),
      );

      await this.sfcRepository.saveList(listSFC);
    }

    return;
  }

  public async update(id: number, dto: UpdateSFCDto): Promise<SFCEntity> {
    const existingSFC = await this.getOrFailById(id);

    existingSFC.name = dto.name;
    existingSFC.version = dto.version;

    return this.sfcRepository.save(existingSFC);
  }

  public async findById(id: number): Promise<SFCEntity> {
    return this.sfcRepository.getById(id);
  }

  public async findAll(): Promise<SFCEntity[]> {
    return this.sfcRepository.findAll();
  }

  public async delete(id: number): Promise<void> {
    await this.getOrFailById(id);
    await this.sfcRepository.delete(id);
  }

  public async getList(
    pagination: PageOptionsDto,
  ): Promise<PageDto<SFCEntity>> {
    return this.sfcRepository.findBy(pagination);
  }

  public async getOrFailById(id: number): Promise<SFCEntity> {
    const existingSFC = await this.findById(id);
    if (!existingSFC) {
      throw new SFCByIdNotFoundException();
    }

    return existingSFC;
  }

  private buildSFC(
    name: string,
    version: string,
    device: DeviceEntity,
  ): SFCEntity {
    const sfc = new SFCEntity();

    sfc.name = name;
    sfc.version = version;
    sfc.device = device;

    return sfc;
  }
}
