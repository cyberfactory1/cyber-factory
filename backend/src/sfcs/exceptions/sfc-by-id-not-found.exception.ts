import { NotFoundException } from '@nestjs/common';

export class SFCByIdNotFoundException extends NotFoundException {
  constructor() {
    super(`Structural functional characteristic with id not found`);
  }
}
