import { Column, Entity, JoinColumn, ManyToOne, Unique } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';

@Entity({ name: 'sfcs' })
@Unique(['name', 'version', 'deviceId'])
export class SFCEntity extends AbstractEntity {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  version: string;

  @Column()
  deviceId!: number;

  @ManyToOne(() => DeviceEntity, (device) => device.sfcs)
  @JoinColumn()
  device!: DeviceEntity;
}
