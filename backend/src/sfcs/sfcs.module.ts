import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { DevicesModule } from '@/devices/devices.module';

import { SFCController } from './controllers/sfcs.controller';
import { SFCEntity } from './dao/entity/sfc.entity';
import { PostgresSFCRepository } from './repositories/postgres-sfc.repository';
import { SFCRepository } from './repositories/sfc.repository';
import { SFCService } from './services/sfc.service';

@Module({
  imports: [TypeOrmModule.forFeature([SFCEntity]), DevicesModule],
  providers: [
    SFCService,
    {
      provide: SFCRepository,
      useClass: PostgresSFCRepository,
    },
  ],
  exports: [SFCService, SFCRepository],
  controllers: [SFCController],
})
export class SFCModule {}
