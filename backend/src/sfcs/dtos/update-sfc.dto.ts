import { PartialType } from '@nestjs/swagger';

import { SFCItemDto } from '@/sfcs/dtos';

export class UpdateSFCDto extends PartialType(SFCItemDto) {}
