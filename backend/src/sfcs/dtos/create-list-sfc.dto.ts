import { DeviceMacAddressDto } from '@/devices/dtos';
import { SFCItemDto } from '@/sfcs/dtos';

export class CreateListSFCDto extends DeviceMacAddressDto {
  public readonly items: SFCItemDto[];
}
