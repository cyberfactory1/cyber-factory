import { Module } from '@nestjs/common';

import { BcryptService } from '@/bcrypt/services/bcrypt.service';

@Module({
  imports: [],
  providers: [BcryptService],
  exports: [BcryptService],
})
export class BcryptModule {}
