import { ConfigNamespacesEnum } from '@common/constants/config-namespaces.enum';
import { IJwtConfig } from '@configs/jwt-config';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

import { JwtTokenPayload } from '@/tokens/abstracts/jwt-token-payload';
import { MAX_SIZE_TOKENS_PAIR } from '@/tokens/constants';
import { TokenPairEntity } from '@/tokens/dao/entity/token-pair.entity';
import { CreateTokenDto } from '@/tokens/dtos';
import { TokensByRefreshTokenNotFoundException } from '@/tokens/exceptions';
import { TokensPairsRepository } from '@/tokens/repositories/tokens-pairs.repository';
import { UserEntity } from '@/users/dao/entity/user.entity';

@Injectable()
export class TokensService {
  constructor(
    private readonly tokensRepository: TokensPairsRepository,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  public async create(dto: CreateTokenDto): Promise<TokenPairEntity> {
    const countTokens = await this.getCountTokensByUserId(dto.userId);
    if (countTokens && countTokens >= MAX_SIZE_TOKENS_PAIR) {
      await this.tokensRepository.deleteByUserId(dto.userId);
    }

    const tokenPair = this.buildTokenPair(
      dto.accessToken,
      dto.refreshToken,
      dto.userId,
    );

    return this.tokensRepository.save(tokenPair);
  }

  public async findByRefreshToken(
    refreshToken: string,
  ): Promise<TokenPairEntity> {
    return this.tokensRepository.findByRefreshToken(refreshToken);
  }

  public async deleteToken(refreshToken: string): Promise<void> {
    const findToken = await this.findByRefreshToken(refreshToken);
    if (!findToken) {
      throw new TokensByRefreshTokenNotFoundException();
    }

    await this.tokensRepository.delete(findToken.id);
  }

  public async generateTokens(user: UserEntity): Promise<TokenPairEntity> {
    const payload = this.buildPayload(user);
    const accessToken = await this.generateAccessToken(payload);
    const refreshToken = await this.generateRefreshToken(payload);

    return this.create(
      this.buildCreateTokenDto(accessToken, refreshToken, user.id),
    );
  }

  public async validateAccessToken(
    token: string,
  ): Promise<JwtTokenPayload | null> {
    return this.validateToken(token, this.jwtConfig.accessTokenSecret);
  }

  public async validateRefreshToken(
    token: string,
  ): Promise<JwtTokenPayload | null> {
    return this.validateToken(token, this.jwtConfig.refreshTokenSecret);
  }

  private readonly jwtConfig = this.configService.get<IJwtConfig>(
    ConfigNamespacesEnum.JWT,
  );

  private async generateAccessToken(payload: JwtTokenPayload): Promise<string> {
    return this.jwtService.signAsync(payload, {
      secret: this.jwtConfig.accessTokenSecret,
      expiresIn: this.jwtConfig.accessTokenExpiration,
    });
  }

  private async generateRefreshToken(
    payload: JwtTokenPayload,
  ): Promise<string> {
    return this.jwtService.signAsync(payload, {
      secret: this.jwtConfig.refreshTokenSecret,
      expiresIn: this.jwtConfig.refreshTokenExpiration,
    });
  }

  private async validateToken(
    token: string,
    secret: string,
  ): Promise<JwtTokenPayload | null> {
    try {
      const userData: JwtTokenPayload = await this.jwtService.verifyAsync(
        token,
        {
          secret,
        },
      );

      return userData;
    } catch (e) {
      return null;
    }
  }

  private async getCountTokensByUserId(userId: number): Promise<number> {
    const [, count] = await this.tokensRepository.findByUserId(userId);
    return count;
  }

  private buildTokenPair(
    accessToken: string,
    refreshToken: string,
    userId: number,
  ): TokenPairEntity {
    const tokenPair = new TokenPairEntity();

    tokenPair.accessToken = accessToken;
    tokenPair.refreshToken = refreshToken;
    tokenPair.userId = userId;

    return tokenPair;
  }

  private buildPayload(user: UserEntity): JwtTokenPayload {
    return {
      id: user.id,
      email: user.email,
      roles: user.roles,
    };
  }

  private buildCreateTokenDto(
    accessToken: string,
    refreshToken: string,
    userId: number,
  ): CreateTokenDto {
    return {
      accessToken,
      refreshToken,
      userId,
    };
  }
}
