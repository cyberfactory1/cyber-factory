import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { UserEntity } from '@/users/dao/entity/user.entity';

@Entity({ name: 'tokens_pairs' })
export class TokenPairEntity extends AbstractEntity {
  @Column()
  accessToken: string;

  @Column()
  refreshToken: string;

  @Column()
  userId!: number;

  @ManyToOne(() => UserEntity, (user) => user.tokens)
  @JoinColumn()
  user!: UserEntity;
}
