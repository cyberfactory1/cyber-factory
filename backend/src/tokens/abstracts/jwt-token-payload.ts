import { RoleEntity } from '@/roles/dao/entity/role.entity';

export type JwtTokenPayload = {
  id: number;
  email: string;
  roles: RoleEntity[];
};
