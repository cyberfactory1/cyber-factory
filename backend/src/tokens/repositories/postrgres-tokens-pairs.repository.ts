import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { TokenPairEntity } from '@/tokens/dao/entity/token-pair.entity';
import { TokensPairsRepository } from '@/tokens/repositories/tokens-pairs.repository';

@Injectable()
export class PostgresTokensPairsRepository extends TokensPairsRepository {
  constructor(
    @InjectRepository(TokenPairEntity)
    private readonly tokenPairRepository: Repository<TokenPairEntity>,
  ) {
    super(tokenPairRepository);
  }

  public async findByRefreshToken(token: string): Promise<TokenPairEntity> {
    return this.tokenPairRepository.findOne({
      where: {
        refreshToken: token,
      },
    });
  }

  public async deleteByUserId(userId: number): Promise<void> {
    const tokens = await this.tokenPairRepository.find({
      where: {
        userId,
      },
    });

    await this.tokenPairRepository.remove(tokens);
  }

  public async findByUserId(
    userId: number,
  ): Promise<[TokenPairEntity[], number]> {
    return this.tokenPairRepository.findAndCount({
      where: {
        userId: userId,
      },
    });
  }
}
