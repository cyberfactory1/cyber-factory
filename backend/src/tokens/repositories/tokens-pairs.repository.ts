import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { TokenPairEntity } from '@/tokens/dao/entity/token-pair.entity';

export abstract class TokensPairsRepository extends AbstractRepository<TokenPairEntity> {
  public abstract findByRefreshToken(token: string): Promise<TokenPairEntity>;

  public abstract deleteByUserId(userId: number): Promise<void>;

  public abstract findByUserId(
    userId: number,
  ): Promise<[TokenPairEntity[], number]>;
}
