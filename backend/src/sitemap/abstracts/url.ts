import { Freq } from '@/sitemap/abstracts/freq';

export interface Url {
  loc: string;
  lastmod: string;
  changefreq: Freq;
  priority: string;
}
