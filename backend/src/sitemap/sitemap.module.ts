import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { SitemapController } from '@/sitemap/sitemap.controller';
import { SitemapService } from '@/sitemap/sitemap.service';

@Module({
  controllers: [SitemapController],
  providers: [SitemapService],
  imports: [ConfigModule],
})
export class SitemapModule {}
