import { format, subDays } from 'date-fns';

export const getLastModificationDate = () => {
  const formatString = "yyyy-MM-dd'T'HH:mm:00:000xxx";

  return format(subDays(new Date(), 1), formatString);
};
