import { Controller, Get, Header } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Builder } from 'xml2js';

import { SitemapService } from '@/sitemap/sitemap.service';

@Controller()
export class SitemapController {
  private readonly DOMAIN = 'DOMAIN';
  public domain: string;

  constructor(
    private readonly configService: ConfigService,
    private readonly sitemapService: SitemapService,
  ) {
    this.domain = this.configService.get(this.DOMAIN) ?? '';
  }

  @Get('xml')
  @Header('content-type', 'text/xml')
  async sitemap() {
    const builder = new Builder({
      xmldec: {
        version: '1.0',
        encoding: 'UTF-8',
      },
    });

    return builder.buildObject({
      urlset: {
        $: {
          xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
        },
        url: await this.sitemapService.getUrl(this.domain),
      },
    });
  }
}
