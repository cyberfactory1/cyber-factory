import { Injectable } from '@nestjs/common';

import { Freq } from '@/sitemap/abstracts/freq';
import { Url } from '@/sitemap/abstracts/url';
import { getLastModificationDate } from '@/sitemap/helpers/get-last-modification-date';
import { URLS } from '@/sitemap/sitemap.constants';

@Injectable()
export class SitemapService {
  public async getUrl(domain: string) {
    const url = [
      {
        loc: domain,
        lastmod: getLastModificationDate(),
        changefreq: Freq.daily,
        priority: '1.0',
      },
    ];

    for (const [key, value] of Object.entries(URLS)) {
      url.push({
        loc: value,
        lastmod: getLastModificationDate(),
        changefreq: Freq.daily,
        priority: '1.0',
      });
    }

    return url;
  }

  public formUrl(domain: string, path: string, pageName: string): Url {
    return {
      loc: `${domain}${path}/${pageName}`,
      lastmod: getLastModificationDate(),
      changefreq: Freq.daily,
      priority: '1.0',
    };
  }
}
