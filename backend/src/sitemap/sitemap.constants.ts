export const URLS: Record<string, string> = {
  sfc: '/api/v1/sfc',
  systemServices: '/api/v1/system-services',
  applications: '/api/v1/applications',
  networkInterfaces: '/api/v1/network-interfaces',
  monitorResources: '/api/v1/monitor-resources',
  arpTable: '/api/v1/arp-table',
  countPackets: '/api/v1/count-packets',
  hosts: '/api/v1/hosts',
  ports: '/api/v1/ports',
};
