import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
} from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { CPSEntity } from '@/cpss/dao/entity/cps.entity';
import { RoleEntity } from '@/roles/dao/entity/role.entity';
import { TokenPairEntity } from '@/tokens/dao/entity/token-pair.entity';

@Entity({ name: 'users' })
export class UserEntity extends AbstractEntity {
  @Column({ unique: true, type: 'varchar' })
  email: string;

  @Column({ type: 'varchar' })
  passwordHash: string;

  @ManyToMany(() => RoleEntity, (RoleEntity) => RoleEntity.users, {
    eager: true,
  })
  @JoinTable()
  roles!: RoleEntity[];

  @ManyToMany(() => CPSEntity, (cpsEntity) => cpsEntity.users)
  @JoinTable()
  cpss!: CPSEntity[];

  @OneToMany(() => TokenPairEntity, (token) => token.user, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  tokens!: TokenPairEntity[];
}
