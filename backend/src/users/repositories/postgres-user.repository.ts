import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserEntity } from '@/users/dao/entity/user.entity';
import { UsersRepository } from '@/users/repositories/users.repository';

@Injectable()
export class PostgresUsersRepository extends UsersRepository {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
  ) {
    super(usersRepository);
  }

  public async findByEmail(email: string): Promise<UserEntity> {
    return this.usersRepository.findOneBy({ email });
  }
}
