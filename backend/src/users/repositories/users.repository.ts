import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { UserEntity } from '@/users/dao/entity/user.entity';

export abstract class UsersRepository extends AbstractRepository<UserEntity> {
  public abstract findByEmail(email: string): Promise<UserEntity>;
}
