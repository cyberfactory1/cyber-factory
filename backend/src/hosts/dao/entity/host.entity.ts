import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  Unique,
} from 'typeorm';

import { AbstractEntity } from '@/common/entities/abstract-entity';
import { DeviceEntity } from '@/devices/dao/entity/device.entity';
import { PortEntity } from '@/ports/dao/entity/ports.entity';

@Entity({ name: 'hosts' })
@Unique(['ipAddress', 'deviceId'])
export class HostEntity extends AbstractEntity {
  @Column({ type: 'varchar' })
  ipAddress: string;

  @Column({ type: 'varchar' })
  macAddress: string;

  @Column()
  deviceId!: number;

  @ManyToOne(() => DeviceEntity, (device) => device.systemServices)
  @JoinColumn()
  device!: DeviceEntity;

  @OneToMany(() => PortEntity, (PortEntity) => PortEntity.host, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  ports!: PortEntity[];
}
