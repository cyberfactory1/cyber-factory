import { MigrationInterface, QueryRunner } from 'typeorm';

export class DeleteMacAddressFromIndexHostsTable1690217931519
  implements MigrationInterface
{
  name = 'DeleteMacAddressFromIndexHostsTable1690217931519';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "hosts" DROP CONSTRAINT "UQ_405513c14f4f0503e2f27a5231c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "hosts" ADD CONSTRAINT "UQ_79eb4d00f2aa2c1b6be3711b683" UNIQUE ("ip_address", "device_id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "hosts" DROP CONSTRAINT "UQ_79eb4d00f2aa2c1b6be3711b683"`,
    );
    await queryRunner.query(
      `ALTER TABLE "hosts" ADD CONSTRAINT "UQ_405513c14f4f0503e2f27a5231c" UNIQUE ("ip_address", "mac_address", "device_id")`,
    );
  }
}
