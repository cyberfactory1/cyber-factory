import { NotFoundException } from '@nestjs/common';

export class HostByIpAddressFoundException extends NotFoundException {
  constructor() {
    super(`Host with ip address not found`);
  }
}
