import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PageDto } from '@/common/pagination/page.dto';
import { PageMetaDto } from '@/common/pagination/page-meta.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { HostEntity } from '@/hosts/dao/entity/host.entity';
import { HostsRepository } from '@/hosts/repositories/hosts.repository';

@Injectable()
export class PostgresHostsRepository extends HostsRepository {
  constructor(
    @InjectRepository(HostEntity)
    private readonly hostsRepository: Repository<HostEntity>,
  ) {
    super(hostsRepository);
  }

  public async saveList(listHosts: HostEntity[]): Promise<void> {
    const queryBuilder = this.hostsRepository.createQueryBuilder();

    queryBuilder
      .insert()
      .into(HostEntity)
      .values(listHosts)
      .orIgnore()
      .execute();

    return;
  }

  public async findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<HostEntity>> {
    const queryBuilder = this.hostsRepository.createQueryBuilder('hosts');

    queryBuilder
      .orderBy('hosts.created_date', pagination.order)
      .skip(pagination.skip)
      .take(pagination.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({
      pageOptionsDto: pagination,
      itemCount,
    });

    return new PageDto(entities, pageMetaDto);
  }

  public async findByUniqueFields(
    ipAddress: string,
    deviceId?: number,
  ): Promise<HostEntity> {
    return this.hostsRepository.findOne({
      where: {
        ipAddress,
        deviceId,
      },
    });
  }
}
