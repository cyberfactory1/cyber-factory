import { PageDto } from '@/common/pagination/page.dto';
import { PageOptionsDto } from '@/common/pagination/page-options.dto';
import { AbstractRepository } from '@/common/repositories/abstract.repository';
import { HostEntity } from '@/hosts/dao/entity/host.entity';

export abstract class HostsRepository extends AbstractRepository<HostEntity> {
  public abstract saveList(listHosts: HostEntity[]): Promise<void>;

  public abstract findBy(
    pagination: PageOptionsDto,
  ): Promise<PageDto<HostEntity>>;

  public abstract findByUniqueFields(
    ipAddress: string,
    deviceId?: number,
  ): Promise<HostEntity>;
}
