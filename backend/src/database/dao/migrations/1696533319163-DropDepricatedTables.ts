import { MigrationInterface, QueryRunner } from 'typeorm';

export class DropDepricatedTables1696533319163 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `DROP TABLE IF EXISTS "structural_functional_characteristics";`,
    );

    await queryRunner.query(
      `DROP TABLE IF EXISTS "users_cyber_physical_systems_cyber_physical_systems";`,
    );

    await queryRunner.query(`DROP TABLE IF EXISTS "cyber_physical_systems";`);
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public async down(queryRunner: QueryRunner): Promise<void> {}
}
