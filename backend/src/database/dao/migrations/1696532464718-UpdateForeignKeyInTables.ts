import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateForeignKeyInTables1696532464718
  implements MigrationInterface
{
  name = 'UpdateForeignKeyInTables1696532464718';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "devices" DROP CONSTRAINT "FK_5c5d96b61fca711f2d6a914eeb4"`,
    );
    await queryRunner.query(
      `ALTER TABLE "devices" RENAME COLUMN "cyber_physical_system_id" TO "cps_id"`,
    );
    await queryRunner.query(
      `CREATE TABLE "cpss" ("id" SERIAL NOT NULL, "created_date" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "description" character varying NOT NULL, CONSTRAINT "PK_fa0312e37fdb346e51e673995e0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "sfcs" ("id" SERIAL NOT NULL, "created_date" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "version" character varying NOT NULL, "device_id" integer NOT NULL, CONSTRAINT "UQ_a181796bd287714ed409768abda" UNIQUE ("name", "version", "device_id"), CONSTRAINT "PK_0a8384839a2094d82368773cf99" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users_cpss_cpss" ("users_id" integer NOT NULL, "cpss_id" integer NOT NULL, CONSTRAINT "PK_b23144d3676bd83807defbd9194" PRIMARY KEY ("users_id", "cpss_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_ae177d41b7a1061a55ebf1e121" ON "users_cpss_cpss" ("users_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_8afb5e5d2023624ff959bdfc15" ON "users_cpss_cpss" ("cpss_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "sfcs" ADD CONSTRAINT "FK_d92fabf5c73695b4800a2e4f834" FOREIGN KEY ("device_id") REFERENCES "devices"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "devices" ADD CONSTRAINT "FK_19f6a6dd60a33216ec12c65931f" FOREIGN KEY ("cps_id") REFERENCES "cpss"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_cpss_cpss" ADD CONSTRAINT "FK_ae177d41b7a1061a55ebf1e1219" FOREIGN KEY ("users_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_cpss_cpss" ADD CONSTRAINT "FK_8afb5e5d2023624ff959bdfc15f" FOREIGN KEY ("cpss_id") REFERENCES "cpss"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users_cpss_cpss" DROP CONSTRAINT "FK_8afb5e5d2023624ff959bdfc15f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "users_cpss_cpss" DROP CONSTRAINT "FK_ae177d41b7a1061a55ebf1e1219"`,
    );
    await queryRunner.query(
      `ALTER TABLE "devices" DROP CONSTRAINT "FK_19f6a6dd60a33216ec12c65931f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "sfcs" DROP CONSTRAINT "FK_d92fabf5c73695b4800a2e4f834"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_8afb5e5d2023624ff959bdfc15"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_ae177d41b7a1061a55ebf1e121"`,
    );
    await queryRunner.query(`DROP TABLE "users_cpss_cpss"`);
    await queryRunner.query(`DROP TABLE "sfcs"`);
    await queryRunner.query(`DROP TABLE "cpss"`);
    await queryRunner.query(
      `ALTER TABLE "devices" RENAME COLUMN "cps_id" TO "cyber_physical_system_id"`,
    );
    await queryRunner.query(
      `ALTER TABLE "devices" ADD CONSTRAINT "FK_5c5d96b61fca711f2d6a914eeb4" FOREIGN KEY ("cyber_physical_system_id") REFERENCES "cyber_physical_systems"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
